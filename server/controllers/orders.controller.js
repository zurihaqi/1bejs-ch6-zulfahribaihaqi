const { order, customer, product } = require('../db/models');

module.exports = class orderController{
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        },
        include: [{
            model: customer,
            attributes:{
                exclude: ['createdAt', 'updatedAt']
            }
        }],
    };

    static async getAllorders(req, res){
        try{
            let {skip, row} = req.query;
    
            if(skip) orderController.options.offset = +skip - 1;
            if(row) orderController.options.limit = +row;
    
            const allorders = await order.findAll(orderController.options);
    
            return res.status(200).json({
                status: 'Success',
                data: allorders
            });
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async getorderById(req, res){
        try{
            const found = await order.findByPk(req.params.id, orderController.options);
    
            if(found){
                return res.status(200).json({
                    status: 'Success',
                    data: found
                });
            };
            throw ({code: 404, status: 'Not found', message: `Order dengan id ${req.params.id} tidak ditemukan.`});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async createorder(req, res){
        try{
            const {quantity, order_date, customerId} = req.body;
            const custExists = await customer.findByPk(customerId);
            if(!custExists){
                throw ({code: 404, status: 'Not found', message: `Order dengan id ${customerId} tidak ditemukan.`});
            };
            const created = await order.create({
                quantity: quantity,
                order_date: order_date,
                customerId: customerId
            });
            return res.status(200).json({
                status: 'Berhasil membuat data.',
                data: created
            });
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async updateorder(req, res){
        try{
            const {quantity, order_date, customerId} = req.body;
            const custExists = await customer.findByPk(customerId);
            if(!custExists){
                throw ({code: 404, status: 'Not found', message: `Order dengan id ${customerId} tidak ditemukan.`});
            };
            const updated = await order.update({
                quantity: quantity,
                order_date: order_date,
                customerId: customerId
            }, {
                where: {
                    id: req.params.id
                }
            });
            if(updated){
                const updatedOrder = await order.findByPk(req.params.id, orderController.options);
                if(updatedOrder){
                    return res.status(200).json({
                        status: `Berhasil mengupdate data order dengan id ${req.params.id}`,
                        data: updatedOrder
                    });
                };
                throw ({code: 404, status: 'Not found', message: `Order dengan id ${req.params.id} tidak ditemukan.`});
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async deleteorder(req, res){
        try{
            const foundFK = await product.findOne({where: {orderId: req.params.id}});
            if(foundFK){
                throw ({code: 422, status: 'Conflict', message: `Order dengan id ${req.params.id} masih terpakai pada data produk berikut: ${foundFK}`});
            };
            const deleted = await order.destroy({
                where: {id: req.params.id}
            });
            if(deleted) return res.status(200).json({
                status: 'Success',
                message: `Order dengan id ${req.params.id} berhasil dihapus.`
            });
            throw ({code: 404, status: 'Not found', message: `Order dengan id ${req.params.id} tidak ditemukan.`});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
};