const { video } = require('../db/models');

module.exports = class videoController{
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        },
    };

    static async videoUpload(req, res){
        try{
            const uploadedVideo = await video.create({video_name: req.body.uploadResult.secure_url});
            if(uploadedVideo){
                return res.status(200).json({
                    status: "Success",
                    data: {
                        id: uploadedVideo.id,
                        video: uploadedVideo.video_name
                    }
                });
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async getVideo(req, res){
        try{
            const foundVideo = await video.findByPk(req.params.id, videoController.options);
            if(foundVideo){
                return res.status(200).json({
                    status: 'Success',
                    video: foundVideo.video_name
                });
            };
            throw({code: 404, status: 'Not found', message: `Video dengan id ${req.params.id} tidak ditemukan`});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
};