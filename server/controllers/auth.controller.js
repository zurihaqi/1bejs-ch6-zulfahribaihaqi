const { user } = require('../db/models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { JWT_SECRET, SALT_ROUNDS } = process.env;
const emailTransporter = require('../middlewares/mailer');

module.exports = class authController{
    static async login(req, res){
        try{
            const { email, password } = req.body;
            const foundUser = await user.findOne({
                where: {
                    email: email
                }
            });
            if(!foundUser) throw ({code: 404, status: 'Not found', message: 'Email belum terdaftar'});
            const isValidPassword = bcrypt.compareSync(password, foundUser.password);
            if(isValidPassword){
                const payload = {
                    id: foundUser.id,
                    name: foundUser.name,
                    profilepic: foundUser.profilepic,
                    email: foundUser.email
                };
                const token = jwt.sign(payload, JWT_SECRET, {expiresIn: '1h'});
                return res.status(200).json({
                    status: "Login success",
                    type: "Bearer Token",
                    token: token
                });
            };
            throw ({code: 401, status: 'Unauthorized', message: 'Email atau password salah'});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            }
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async register(req, res){
        try{
            const { name, email, password, passwordConfirmation } = req.body;
            if(password !== passwordConfirmation) throw ({code: 400, status: "Error", message: "Password tidak sama."});
            const emailExists = await user.findOne({where: {email: email}});
            if(emailExists) throw ({code: 409, status: 'Conflict', message: 'Alamat email sudah terdaftar'});
            const createdUser = await user.create({
                name: name,
                email: email,
                password: password
            });
            if(createdUser){
                await emailTransporter(email, 'Pendaftaran Akun API Zul', 'Terima kasih telah mendaftar!');
                return res.status(200).json({
                    status: 'Akun berhasil dibuat.',
                });
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            }
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async forgotPass (req, res) {
        try{
            const { email, newPassword, newPasswordConfirmation, otp } = req.body;
            if(newPassword === newPasswordConfirmation){
                const getUserData = await user.findOne({where: {email: email}});
                if(getUserData){
                    const decryptOTP = bcrypt.compareSync(otp.toUpperCase(), getUserData.otp);
                    if(decryptOTP){
                        const updatedPassword = await getUserData.update({
                            password: newPassword,
                            otp: null
                        },{
                            individualHooks: true
                        });
                        if(updatedPassword){
                            return res.status(200).json({
                                status: "Success",
                                message: "Password berhasil diubah."
                            });
                        };
                    };
                    throw({code: 400, status: "Error", message: "Kode OTP salah."});
                };
                throw({code: 404, status: "Not found", message: "Email belum terdaftar."});
            };
            throw({code: 400, status: "Error", message: "Password tidak sama."});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async otp (req, res) {
        try{
            const { email } = req.body;
            const otp = Math.random().toString(36).slice(-6).toUpperCase();
            const hashedOtp = bcrypt.hashSync(otp, +SALT_ROUNDS);
            const insertOTP = await user.update(
                {
                    otp: hashedOtp
                },
                {
                    where: {
                        email: email
                    }
                }
            );
            if (!insertOTP[0]) throw { message: 'Email belum terdaftar', status: 'Not found', code: 404 };
            const otpMessage = `Kode otp anda adalah ${otp}.`;
            const emailResponse = await emailTransporter(email, 'OTP Request API Zul', otpMessage);
            return res.status(200).json({
                message: `Kode OTP berhasil dikirim ke ${emailResponse.accepted.join(',').split(',')}. Gunakan kode OTP untuk menghapus akun atau mengubah password.`
            });
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        }; 
    };
};