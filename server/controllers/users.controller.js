const { user } = require('../db/models');
const bcrypt = require('bcrypt');
const emailTransporter = require('../middlewares/mailer');

module.exports = class userController {
    static async whoAmI(req, res){
        try{
            const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
            if(!currentUser.profilepic){
                currentUser.profilepic = "Belum ada";
            };
            if(currentUser){
                return res.status(200).json({
                    status: "Success",
                    data: {
                        name: currentUser.name,
                        email: currentUser.email,
                        profilepic: currentUser.profilepic
                    }
                });
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async editUserData(req, res){
        try{
            const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
            const { name, email, password } = req.body;
            const emailExists = await user.findOne({where: {email: email}});
            const getUserData = await user.findOne({where: {id: currentUser.id}});
            const isValidPassword = bcrypt.compareSync(password, getUserData.password);
            if(emailExists){
                if(emailExists.email !== currentUser.email){
                    throw({code: 409, status: 'Duplicate', message: 'Email sudah terdaftar.'});
                };
            };
            if(isValidPassword){
                const updated = await getUserData.update({
                    name: name,
                    email: email,
                    password: password
                },{
                    individualHooks: true
                });
                if(updated){
                    const updatedUser = await user.findOne({where: {email: email}});
                    if(updatedUser){
                        await emailTransporter(
                            email, 
                            'Perubahan data profil pada API Zul', 
                            `Email ini dikirim karena data profil telah diubah. Data profil baru:
Nama: ${updatedUser.name}
Email: ${updatedUser.email}`);
                        return res.status(200).json({
                            status: "Success",
                            message: "Berhasil mengubah data profil dengan data berikut. Lakukan login kembali",
                            data: {
                                name: updatedUser.name,
                                email: updatedUser.email
                            }
                        });
                    };
                    throw({code: 404, status: 'Not found', message: `User dengan email ${email} tidak ditemukan`});
                };
            };
            throw({code: 400, status: "Error", message: "Password salah."})
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async uploadProfilePic (req, res) {
        try{
            const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
            const uploadPic = await user.update({
                profilepic: req.body.uploadResult.secure_url
            },{
                where: {
                    id: currentUser.id
                }
            });
            if(uploadPic){
                const getUserData = await user.findOne({where: { id: currentUser.id }})
                return res.status(200).json({
                    status: "Sukses mengunggah foto profil. Lakukan login kembali.",
                    profilepic: getUserData.profilepic
                });
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };

    static async deleteAccount (req, res) {
        try{
            const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
            const { otp } = req.body;
            const foundOTP = await user.findOne({where: {id: currentUser.id}});
            const decryptOTP = bcrypt.compareSync(otp.toUpperCase(), foundOTP.otp);
            if(!decryptOTP) throw ({code: 400, status: 'Error', message: 'Kode OTP salah.'});

            const deleted = await user.destroy({
                where: {
                    id: currentUser.id
                }
            });
            if(deleted){
                return res.status(200).json({
                    status: 'Akun berhasil dihapus. Lakukan login kembali.'
                });
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
};