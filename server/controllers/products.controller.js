const { product, order } = require('../db/models');

module.exports = class productController{
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        },
        include: [{
            model: order,
            attributes:{
                exclude: ['createdAt', 'updatedAt']
            }
        }],
    }

    static async getAllproducts(req, res){
        try{
            let {skip, row} = req.query;
    
            if(skip) productController.options.offset = +skip - 1;
            if(row) productController.options.limit = +row;
    
            const allproducts = await product.findAll(productController.options);
    
            return res.status(200).json({
                status: 'Success',
                data: allproducts
            });
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async getproductById(req, res){
        try{
            const found = await product.findByPk(req.params.id, productController.options);
    
            if(found){
                return res.status(200).json({
                    status: 'Success',
                    data: found
                });
            };
            throw({code: 404, status: 'Not found', message: `Product dengan id ${req.params.id} tidak ditemukan.`});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async createproduct(req, res){
        try{
            const {product_name, price, orderId} = req.body;
            const productExists = await product.findOne({where: {product_name: product_name}});
            if(productExists){
                throw({code: 409, status: 'Duplicate', message: `Produk dengan nama ${product_name} sudah terdaftar dalam database.`});
            };
            const created = await product.create({
                product_name: product_name,
                price: price,
                orderId: orderId
            });
            return res.status(200).json({
                status: 'Berhasil membuat data.',
                data: created
            });
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async updateproduct(req, res){
        try{
            const {product_name, price, orderId} = req.body;
            const orderExists = await order.findByPk(orderId);
            if(!orderExists){
                throw({code: 404, status: 'Not found', message: `Order dengan id ${orderId} tidak ada dalam database.`});
            };
            const updated = await product.update({
                product_name: product_name,
                price: price,
                orderId: orderId
            }, {
                where: {
                    id: req.params.id
                }
            });
            if(updated){
                const updatedProduct = await product.findByPk(req.params.id, productController.options);
                if(updatedProduct){
                    return res.status(200).json({
                        status: `Berhasil mengupdate data product dengan id ${req.params.id}`,
                        data: updatedProduct
                    });
                };
                throw({code: 404, status: 'Not found', message: `Product dengan id ${req.params.id} tidak ditemukan.`});
            };
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
    
    static async deleteproduct(req, res){
        try{
            const deleted = await product.destroy({
                where: {id: req.params.id}
            });
            if(deleted) return res.status(200).json({
                status: 'Success',
                message: `Product dengan id ${req.params.id} berhasil dihapus.`
            });
            throw({code: 404, status: 'Not found', message: `Product dengan id ${req.params.id} tidak ditemukan.`});
        }catch(error){
            if(error.code){
                return res.status(error.code).json({
                    status: error.status,
                    message: error.message
                });
            };
            return res.status(500).json({
                status: 'Server error',
                message: error.message
            });
        };
    };
};