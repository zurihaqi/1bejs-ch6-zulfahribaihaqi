'use strict';
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addConstraint('orders', {
      fields: ['customerId'],
      type: 'foreign key',
      name: 'customer_order_association',
      references: {
        table: 'customers',
        field: 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint('orders', 'customer_order_association')
  }
};
