'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addConstraint('products', {
      fields: ['orderId'],
      type: 'foreign key',
      name: 'product_order_association',
      references: {
        table: 'orders',
        field: 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint('products', 'product_order_association')
  }
};
