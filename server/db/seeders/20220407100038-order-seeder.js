'use strict';

const orderData = require('../../masterdata/orders.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const parsedData = orderData.map((eachData) => {
      eachData.order_date = new Date();
      eachData.createdAt = new Date();
      eachData.updatedAt = new Date();
      return eachData;
    });
    await queryInterface.bulkInsert('orders', parsedData);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('orders', null, { truncate: { cascade: true }, restartIdentity: true });
  }
};
