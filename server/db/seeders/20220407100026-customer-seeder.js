'use strict';

const customerData = require('../../masterdata/customers.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const parsedData = customerData.map((eachData) => {
      eachData.email = eachData.email.toLowerCase();
      eachData.createdAt = new Date();
      eachData.updatedAt = new Date();
      return eachData;
    });

    await queryInterface.bulkInsert('customers', parsedData);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('customers', null, { truncate: { cascade: true }, restartIdentity: true });
  }
};
