'use strict';

const bcrypt = require('bcrypt');
const userData = require('../../masterdata/users.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const parsedData = userData.map((eachData) => {
      eachData.createdAt = new Date();
      eachData.updatedAt = new Date();
      eachData.password = bcrypt.hashSync(eachData.password, +process.env.SALT_ROUNDS);
      return eachData;
    });
    await queryInterface.bulkInsert('users', parsedData);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, { truncate: { cascade: true }, restartIdentity: true });
  }
};
