'use strict';

const productData = require('../../masterdata/products.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const parsedData = productData.map((eachData) => {
      eachData.createdAt = new Date();
      eachData.updatedAt = new Date();
      return eachData;
    });

    await queryInterface.bulkInsert('products', parsedData);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('products', null, { truncate: { cascade: true }, restartIdentity: true });
  }
};
