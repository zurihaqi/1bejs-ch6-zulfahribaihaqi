const multer = require('multer');
const fileSizeLimitErrorHandler = (err, req, res, next) => {
    if (err) {
      return res.status(400).json({
          status: "Error",
          message: "Ukuran file terlalu besar."
      })
    };
    next();
};

const imageStorage = multer.diskStorage({
    filename: (req, file, cb) => {
        const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
        const fileType = file.mimetype.split('/')[1];
        cb(null, Date.now()+ '-' + currentUser.email.replace(/\s/g, '') + '-' + file.fieldname + `.${fileType}`);
    }
});

const videoStorage = multer.diskStorage({
    filename: (req, file, cb) => {
        const currentUser = JSON.parse(atob(req.headers.authorization.split('.')[1]));
        const fileType = file.mimetype.split('/')[1];
        cb(null, Date.now()+ '-' + currentUser.email.replace(/\s/g, '') + '-' + file.fieldname + `.${fileType}`);
    }
});

const imageUpload = multer({
    storage: imageStorage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg'){ 
            return cb(null, true);
        };
        cb(null, false);
        return cb(new Error('Gambar hanya dapat berformat .png, .jpg dan .jpeg'));
    },
    limits: {
        fileSize: 1000000,
        files: 1
    }
});

const videoUpload = multer({
    storage: videoStorage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == 'video/mp4'){ 
            return cb(null, true);
        };
        cb(null, false);
        return cb(new Error('Video hanya dapat berformat .mp4'));
    },
    limits: {
        fileSize: 50000000,
        files: 1
    }
});

module.exports = {
    imageUpload,
    videoUpload,
    fileSizeLimitErrorHandler
};