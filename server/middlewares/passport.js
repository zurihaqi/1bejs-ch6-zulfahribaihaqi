const { JWT_SECRET } = process.env;
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy, ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {};
const { user } = require('../db/models');

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = JWT_SECRET;

passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
        user.findOne({
            where: {
                id: jwt_payload.id,
                name: jwt_payload.name,
                email: jwt_payload.email,
                profilepic: jwt_payload.profilepic
            }
        })
        .then((user) => done(null, user))
        .catch((err) => done(err, false));
    })
);

module.exports = authenticate = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if(err) return next(err);
        if(!user) return res.status(401).json({
            status: 'Unauthorized',
            message: `Lakukan login melalui ${req.headers.host}/api-docs atau postman.`
        });
        req.user = user;
        next();
    })(req, res, next);
};