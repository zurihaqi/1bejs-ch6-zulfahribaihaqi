const { SENDER_EMAIL, CLIENT_ID, CLIENT_SECRET, REDIRECT_URL, REFRESH_TOKEN } = process.env;
const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;

const oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

oauth2Client.setCredentials({
    refresh_token: REFRESH_TOKEN
});
const accessToken = oauth2Client.getAccessToken();

const sendEmail = (receiver, subject, message) => {
    const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
             type: "OAuth2",
             user: SENDER_EMAIL, 
             clientId: CLIENT_ID,
             clientSecret: CLIENT_SECRET,
             refreshToken: REFRESH_TOKEN,
             accessToken: accessToken
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    
    const mailOptions = {
        from: SENDER_EMAIL,
        to: `${receiver}`,
        subject: `${subject}`,
        text: `${message}`
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) reject(err);
            else resolve(info);
        });
    });
};

module.exports = sendEmail;