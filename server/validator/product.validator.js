const { body, validationResult } = require('express-validator');

module.exports.productValidator = [
    body('product_name').isAlphanumeric('en-US', {ignore: ' '}).withMessage('Product name tidak boleh kosong.'),
    body('price').isNumeric().withMessage('Price hanya dapat berupa angka.'),
    body('orderId').isNumeric().withMessage('Order id hanya dapat berupa angka.'),
    (req, res, next) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array()
            });
        };
        next();
    }
];