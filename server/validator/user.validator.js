const { body, validationResult } = require('express-validator');

module.exports = {
    loginValidator: [
        body('email').isEmail().normalizeEmail().withMessage('Harap masukkan email yang sesuai.'),
        body('email').notEmpty().withMessage('Harap masukkan alamat email.'),
        body('password').notEmpty().withMessage('Harap masukkan password.'),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array()
                });
            };
            next();
        }
    ],
    registerValidator: [
        body('name').isAlpha('en-US', {ignore: ' '}).withMessage('Nama tidak boleh mengandung angka atau kosong.'),
        body('email').isEmail().normalizeEmail().withMessage('Harap masukkan alamat email yang sesuai.'),
        body('password').notEmpty().withMessage('Harap masukkan password.'),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array()
                });
            };
            next();
        }
    ],
    updateProfileValidator: [
        body('name').isAlpha('en-US', {ignore: ' '}).withMessage('Nama tidak boleh mengandung angka atau kosong.'),
        body('email').isEmail().normalizeEmail().withMessage('Harap masukkan alamat email yang sesuai.'),
        body('password').notEmpty().withMessage('Harap masukkan password untuk mengubah data'),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array()
                });
            };
            next();
        }
    ],
    forgotPassValidator: [
        body('otp').notEmpty().withMessage('Harap masukkan kode otp.'),
        body('email').isEmail().normalizeEmail().withMessage('Harap masukkan alamat email yang sesuai.'),
        body('newPassword').notEmpty().withMessage('Harap masukkan password.'),
        body('newPasswordConfirmation').notEmpty().withMessage('Harap masukkan password.'),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array()
                });
            };
            next();
        }
    ],
    otpValidator: [
        body('otp').notEmpty().withMessage('Harap masukkan kode otp.'),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array()
                });
            };
            next();
        }
    ]
};