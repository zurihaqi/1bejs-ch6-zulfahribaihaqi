const router = require('express').Router();

//Middlewares import
const logger = require('morgan');
const authenticate  = require('../middlewares/passport');

//Controllers routes import
const { login, register, otp, forgotpass } = require('../routes/auth.routes');
const userRoutes = require('./users.routes');
const customerRoutes = require('./customers.routes');
const orderRoutes = require('./orders.routes')
const productRoutes = require('./products.routes');
const videoRoutes = require('./video.routes');

//Swagger setup
const swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('../../swagger.json');

router.use(logger('combined', { skip: (req, res) => process.env.NODE_ENV === 'test' }));

router.use('/', login);
router.use('/', register);
router.use('/', otp);
router.use('/', forgotpass);
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router.use(authenticate);
router.get('/', (req, res) => {
    return res.status(200).json({
        api_documentation: '/api-docs',
        endpoints: {
            Authentication: {
                register: '/register',
                login: '/login'
            },
            Userpage: {
                endpoint: ['/user', '/user/editprofile'],
                method: {
                    user: ['get', 'post', 'delete'],
                    "user/editprofile": "post"
                }
            },
            Video: {
                endpoint: '/video',
                method: ['get', 'post']
            },
            customers:{
                endpoint: '/customers',
                method: ['get', 'post', 'patch', 'delete']
            }, 
            orders:{
                endpoint: '/orders',
                method: ['get', 'post', 'patch', 'delete']
            }, 
            products:{
                endpoint: '/products',
                method: ['get', 'post', 'patch', 'delete']
            }, 
        }
    });
});
router.use('/user', userRoutes);
router.use('/video', videoRoutes);
router.use('/customers', customerRoutes);
router.use('/orders', orderRoutes);
router.use('/products', productRoutes);

//Page not found handler
router.use((req, res) => {
    return res.status(404).json({
        status: 'Not Found',
        message: 'Alamat tidak ditemukan'
    });
});

module.exports = router;
