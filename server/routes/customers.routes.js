const express = require('express');
const router = express.Router();
const customerController = require('../controllers/customers.controller');
const { cust_validator } = require('../validator/customer.validator');

router.get('/', customerController.getAllCustomers);
router.get('/:id', customerController.getCustomerById);
router.post('/', cust_validator, customerController.createCustomer);
router.patch('/:id', cust_validator, customerController.updateCustomer);
router.delete('/:id', customerController.deleteCustomer);

module.exports = router;