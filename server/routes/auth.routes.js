const router = require('express').Router();
const authController = require('../controllers/auth.controller');
const { loginValidator, registerValidator, forgotPassValidator } = require('../validator/user.validator');

module.exports = {
    login: router.post('/login', loginValidator, authController.login),
    register: router.post('/register', registerValidator, authController.register),
    otp: router.post('/otp', authController.otp),
    forgotpass: router.post('/forgotpass', forgotPassValidator, authController.forgotPass)
};