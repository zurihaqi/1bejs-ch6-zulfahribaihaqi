const router = require('express').Router();
const videoController = require('../controllers/video.controller');
const { videoUpload, fileSizeLimitErrorHandler } = require('../middlewares/multer');
const cloudinaryUpload = require('../middlewares/cloudinary');

router.post('/', videoUpload.single('video'), fileSizeLimitErrorHandler, cloudinaryUpload, videoController.videoUpload);
router.get('/:id', videoController.getVideo);

module.exports = router; 