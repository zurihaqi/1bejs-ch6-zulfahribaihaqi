const router = require('express').Router();
const userController = require('../controllers/users.controller');
const { updateProfileValidator, otpValidator } = require('../validator/user.validator');
const { imageUpload, fileSizeLimitErrorHandler } = require('../middlewares/multer');
const cloudinaryUpload = require('../middlewares/cloudinary');

router.get('/', userController.whoAmI);
router.post('/editprofile', updateProfileValidator, userController.editUserData);
router.post('/', imageUpload.single('profilepic'), fileSizeLimitErrorHandler, cloudinaryUpload, userController.uploadProfilePic);
router.delete('/', otpValidator, userController.deleteAccount);

module.exports = router;