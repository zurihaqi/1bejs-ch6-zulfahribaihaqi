const express = require('express');
const router = express.Router();
const productController = require('../controllers/products.controller');
const { productValidator } = require('../validator/product.validator')

router.get('/', productController.getAllproducts);
router.get('/:id', productController.getproductById);
router.post('/', productValidator, productController.createproduct);
router.patch('/:id', productValidator, productController.updateproduct);
router.delete('/:id', productController.deleteproduct);

module.exports = router;