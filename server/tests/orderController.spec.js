require('dotenv').config();
const app = require('../../index.js');
const request = require('supertest');
const bcrypt = require('bcrypt');
const { sequelize } = require('../db/models');
const { describe, expect, test } = require('@jest/globals');

let token = null;

const testData = 
[
    {
        quantity: 10,
        order_date: "2022-05-14",
        customerId: 2
    },
    {
        quantity: 20,
        order_date: "2022-04-14",
        customerId: 2
    },
    {
        quantity: 5,
        order_date: "2022-04-14",
        customerId: 100
    }
];

const ordersData = require('../masterdata/orders.json').map((eachData) => {
    eachData.order_date = new Date();
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const customersData = require('../masterdata/customers.json').map((eachData) => {
    eachData.email = eachData.email.toLowerCase();
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const usersData = require('../masterdata/users.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    eachData.password = bcrypt.hashSync(eachData.password, +process.env.SALT_ROUNDS);
    return eachData;
});

beforeAll(async () => {
    try{
        await sequelize.queryInterface.bulkInsert('customers', customersData, {});
        await sequelize.queryInterface.bulkInsert('orders', ordersData, {});
        await sequelize.queryInterface.bulkInsert('users', usersData, {});
        const response = await request(app).post('/login').send({
            email: "admin@mail.com",
            password: "admin1234!"
        });
        token = response.body.token;
    }catch(err){
        console.log(err);
    };
});

afterAll(async () => {
    try{
        await sequelize.queryInterface.bulkDelete('users', null, { truncate: { cascade: true }, restartIdentity: true });
        await sequelize.queryInterface.bulkDelete('orders', null, { truncate: { cascade: true }, restartIdentity: true })
        await sequelize.queryInterface.bulkDelete('customers', null, { truncate: { cascade: true }, restartIdentity: true })
    }catch(err){
        console.log(err);
    };
});

describe('Pass getAllOrders function', () => {
    test('Should get all orders data', async () => {
        const response = await request(app).get('/orders').set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(data.length).toBeGreaterThan(0);
        expect(data[0]).toHaveProperty('quantity');
        expect(data[0]).toHaveProperty('order_date');
        expect(data[0]).toHaveProperty('customerId');
    });
});

describe('Pass getOrderById function', () => {
    test('Should get order data by id = 2', async () => {
        const response = await request(app).get('/orders/' + 2).set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(status).toBe('Success');
        expect(response.status).toEqual(200);
        expect(data.quantity).toEqual(1);
        expect(data.customerId).toEqual(1);
    });
});

describe('Fail getOrderById function', () => {
    test('Should fail to get order data by invalid id', async () => {
        const response = await request(app).get('/orders/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Order dengan id 10 tidak ditemukan.')
    });
});

describe('Pass createOrder function', () => {
    test('Should create order with test data', async () => {
        const response = await request(app).post('/orders').set('Authorization', 'Bearer ' + token).send(testData[0]);
        const { status, data } = response.body;
        expect(status).toBe('Berhasil membuat data.');
        expect(response.status).toEqual(200);
        expect(data).toHaveProperty('quantity');
        expect(data.quantity).toEqual(testData[0].quantity);
    });
});

describe('Fail createOrder function', () => {
    test('Should fail to create order with invalid customerId', async () => {
        const response = await request(app).post('/orders').set('Authorization', 'Bearer ' + token).send(testData[2]);
        const { status, message } = response.body;
        expect(status).toBe('Error');
        expect(message).toBe('Customer dengan id 100 tidak ada dalam database.');
        expect(response.status).toEqual(404);
    });
});

describe('Pass updateOrder function', () => {
    test('Should update order with test data', async () => {
        const response = await request(app).patch('/orders/' + 1).set('Authorization', 'Bearer ' + token).send(testData[1]);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Berhasil mengupdate data order dengan id 1');
        expect(data.quantity).toEqual(testData[1].quantity);
    });
});

describe('Fail updateOrder function', () => {
    test('Should fail to update order test data by invalid quantity type', async () => {
        const response = await request(app).patch('/orders/' + 3).set('Authorization', 'Bearer ' + token).send({quantity: 'limabelas'});
        expect(response.status).toEqual(400);
    });
});

describe('Pass deleteOrder function', () => {
    test('Should pass order test data deletion', async () => {
        const response = await request(app).delete('/orders/' + 1).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(message).toBe('Order dengan id 1 berhasil dihapus.')
    });
});

describe('Fail deleteOrder function', () => {
    test('Should fail order test data deletion by invalid id', async () => {
        const response = await request(app).delete('/orders/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Order dengan id 10 tidak ditemukan.')
    });
});