require('dotenv').config();
const app = require('../../index.js');
const request = require('supertest');
const bcrypt = require('bcrypt');
const { sequelize } = require('../db/models');
const { describe, expect, test } = require('@jest/globals');

let token = null;

const testData = 
[
    {
        cust_name: 'Jest',
        email: 'jest@mail.com',
        phone_num: '5432102' 
    },
    {
        cust_name: 'JestDua',
        email: 'jest2@mail.com',
        phone_num: '235478'
    }
];

const customersData = require('../masterdata/customers.json').map((eachData) => {
    eachData.email = eachData.email.toLowerCase();
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const usersData = require('../masterdata/users.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    eachData.password = bcrypt.hashSync(eachData.password, +process.env.SALT_ROUNDS);
    return eachData;
});

beforeAll(async () => {
    try{
        await sequelize.queryInterface.bulkInsert('users', usersData, {});
        await sequelize.queryInterface.bulkInsert('customers', customersData, {});
        const response = await request(app).post('/login').send({
            email: "admin@mail.com",
            password: "admin1234!"
        });
        token = response.body.token;
    }catch(err){
        console.log(err);
    };
});

afterAll(async () => {
    try{
        await sequelize.queryInterface.bulkDelete('users', null, { truncate: { cascade: true }, restartIdentity: true });
        await sequelize.queryInterface.bulkDelete('customers', null, { truncate: { cascade: true }, restartIdentity: true });
    }catch(err){
        console.log(err);
    };
});

describe('Pass getAllCustomers function', () => {
    test('Should get all customers data', async () => {
        const response = await request(app).get('/customers').set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(data.length).toBeGreaterThan(0);
        expect(data[0]).toHaveProperty('id');
        expect(data[0]).toHaveProperty('cust_name');
        expect(data[0]).toHaveProperty('email');
        expect(data[0]).toHaveProperty('phone_num');
    });
});

describe('Pass getCustomerById function', () => {
    test('Should get customer data by id = 2', async () => {
        const response = await request(app).get('/customers/' + 2).set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(data.id).toEqual(2);
        expect(data.cust_name).toEqual('Fahri');
    });
});

describe('Fail getCustomerById function', () => {
    test('Should fail to get customer data by invalid id', async () => {
        const response = await request(app).get('/customers/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Customer dengan id 10 tidak ditemukan.')
    });
});

describe('Pass createCustomer function', () => {
    test('Should create customer with test data', async () => {
        const response = await request(app).post('/customers').set('Authorization', 'Bearer ' + token).send(testData[0]);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Berhasil membuat data.');
        expect(data).toHaveProperty('cust_name');
        expect(data.cust_name).toEqual(testData[0].cust_name);
    });
});

describe('Fail createCustomer function', () => {
    test('Should fail to create duplicate customer test data', async () => {
        const response = await request(app).post('/customers').set('Authorization', 'Bearer ' + token).send(testData[0]);
        const { status, message } = response.body;
        expect(response.status).toEqual(409);
        expect(status).toBe('Error');
        expect(message).toBe('Alamat email sudah terdaftar');
    });
});

describe('Pass updateCustomer function', () => {
    test('Should update customer test data', async () => {
        const response = await request(app).patch('/customers/' + 3).set('Authorization', 'Bearer ' + token).send(testData[1]);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Berhasil mengupdate data customer dengan id 3');
        expect(data.cust_name).toEqual(testData[1].cust_name);
    });
});

describe('Fail updateCustomer function', () => {
    test('Should fail to update customer test data by invalid cust_name', async () => {
        const response = await request(app).patch('/customers/' + 3).set('Authorization', 'Bearer ' + token).send({cust_name: 'Jest2'});
        expect(response.status).toEqual(400);
    });
});

describe('Pass deleteCustomer function', () => {
    test('Should pass customer test data deletion', async () => {
        const response = await request(app).delete('/customers/' + 3).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(message).toBe('Customer dengan id 3 berhasil dihapus.')
    });
});

describe('Fail deleteCustomer function', () => {
    test('Should fail customer test data deletion by invalid id', async () => {
        const response = await request(app).delete('/customers/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Customer dengan id 10 tidak ditemukan.')
    });
});