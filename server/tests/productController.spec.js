require('dotenv').config();
const app = require('../../index.js');
const request = require('supertest');
const bcrypt = require('bcrypt');
const { sequelize } = require('../db/models');
const { describe, expect, test } = require('@jest/globals');

let token = null;

const testData = 
[
    {
        product_name: "Teh Cangkir",
        price: 2000,
        orderId: 1
    },
    {
        product_name: "Samba",
        price: 5000,
        orderId: 2
    },
    {
        product_name: "Barang Mahal",
        price: "limapuluhjuta",
        orderId: 1
    }
];

const productsData = require('../masterdata/products.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const ordersData = require('../masterdata/orders.json').map((eachData) => {
    eachData.order_date = new Date();
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const customersData = require('../masterdata/customers.json').map((eachData) => {
    eachData.email = eachData.email.toLowerCase();
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
});

const usersData = require('../masterdata/users.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    eachData.password = bcrypt.hashSync(eachData.password, +process.env.SALT_ROUNDS);
    return eachData;
});

beforeAll(async () => {
    try{
        await sequelize.queryInterface.bulkInsert('customers', customersData, {});
        await sequelize.queryInterface.bulkInsert('orders', ordersData, {});
        await sequelize.queryInterface.bulkInsert('products', productsData, {});
        await sequelize.queryInterface.bulkInsert('users', usersData, {});
        const response = await request(app).post('/login').send({
            email: "admin@mail.com",
            password: "admin1234!"
        });
        token = response.body.token;
    }catch(err){
        console.log(err);
    };
});

afterAll(async () => {
    try{
        await sequelize.queryInterface.bulkDelete('users', null, { truncate: { cascade: true }, restartIdentity: true });
        await sequelize.queryInterface.bulkDelete('products', null, { truncate: { cascade: true }, restartIdentity: true })
        await sequelize.queryInterface.bulkDelete('orders', null, { truncate: { cascade: true }, restartIdentity: true })
        await sequelize.queryInterface.bulkDelete('customers', null, { truncate: { cascade: true }, restartIdentity: true })
    }catch(err){
        console.log(err);
    };
});

describe('Pass getAllProducts function', () => {
    test('Should get all products data', async () => {
        const response = await request(app).get('/products').set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(data.length).toBeGreaterThan(0);
        expect(data[0]).toHaveProperty('product_name');
        expect(data[0]).toHaveProperty('price');
        expect(data[0]).toHaveProperty('orderId');
    });
});

describe('Pass getProductById function', () => {
    test('Should get product data by id = 1', async () => {
        const response = await request(app).get('/products/' + 1).set('Authorization', 'Bearer ' + token);
        const { status, data } = response.body;
        expect(status).toBe('Success');
        expect(response.status).toEqual(200);
        expect(data.product_name).toEqual('Susu Naga');
        expect(data.price).toEqual(10000);
    });
});

describe('Fail getProductById function', () => {
    test('Should fail to get product data by invalid id', async () => {
        const response = await request(app).get('/products/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Product dengan id 10 tidak ditemukan.')
    });
});

describe('Pass createProduct function', () => {
    test('Should create product with test data', async () => {
        const response = await request(app).post('/products').set('Authorization', 'Bearer ' + token).send(testData[0]);
        const { status, data } = response.body;
        expect(status).toBe('Berhasil membuat data.');
        expect(response.status).toEqual(200);
        expect(data).toHaveProperty('product_name');
        expect(data.price).toEqual(testData[0].price);
    });
});

describe('Fail createProduct function', () => {
    test('Should fail to create product with invalid orderId', async () => {
        const response = await request(app).post('/products').set('Authorization', 'Bearer ' + token).send(testData[2]);
        const { errors } = response.body;
        expect(response.status).toEqual(400);
        expect(errors[0].msg).toBe('Price hanya dapat berupa angka.');
    });
});

describe('Pass updateProduct function', () => {
    test('Should update product with test data', async () => {
        const response = await request(app).patch('/products/' + 1).set('Authorization', 'Bearer ' + token).send(testData[1]);
        const { status, data } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Berhasil mengupdate data product dengan id 1');
        expect(data.product_name).toEqual(testData[1].product_name);
    });
});

describe('Fail updateProduct function', () => {
    test('Should fail to update product test data by invalid price', async () => {
        const response = await request(app).patch('/products/' + 3).set('Authorization', 'Bearer ' + token).send({price: 'limabelasribu'});
        expect(response.status).toEqual(400);
    });
});

describe('Pass deleteProduct function', () => {
    test('Should pass product test data deletion', async () => {
        const response = await request(app).delete('/products/' + 1).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(200);
        expect(status).toBe('Success');
        expect(message).toBe('Product dengan id 1 berhasil dihapus.')
    });
});

describe('Fail deleteProduct function', () => {
    test('Should fail product test data deletion by invalid id', async () => {
        const response = await request(app).delete('/products/' + 10).set('Authorization', 'Bearer ' + token);
        const { status, message } = response.body;
        expect(response.status).toEqual(404);
        expect(status).toBe('Not Found');
        expect(message).toBe('Product dengan id 10 tidak ditemukan.')
    });
});