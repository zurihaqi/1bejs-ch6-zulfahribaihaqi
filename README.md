# https://bejs1-ch6-zulfahribaihaqi.herokuapp.com/api-docs/

<p>
Init db:<br>
sequelize db:create<br>
sequelize db:migrate<br>

Run testing: npm test<br>

Run dev:<br>
sequelize db:seed:all<br>
npm run start:dev<br>

Documentation:<br>
swagger-ui: /api-docs
</p>
